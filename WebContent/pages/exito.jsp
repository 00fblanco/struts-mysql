<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Éxito</title>
</head>
<body>
	<center>
		<h2>Registro grabado con éxito</h2>
		<h4>ID:<s:property value="objTrabajador.id"/></h4>
		<h4>Nombre y Apellido:<s:property value="objTrabajador.NOMBRE"/> <s:property value="objTrabajador.APELLIDO"/></h4>
		<h4>Edad:<s:property value="objTrabajador.edad"/></h4>
		<h4>Genero:<s:property value="objTrabajador.GENERO_COD"/></h4>
		<h4>Departamento:<s:property value="objTrabajador.DEPARTAMENTO_COD"/></h4>
		<a href='<s:url action="inicio"></s:url>'>Registrar nuevo</a>
	</center>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="/struts-tags" prefix="s" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<center>
		<fieldset style="width: 300px">
			<legend>Registro nuevo</legend>
			<s:form action="RegistroNuevo">
				<s:textfield label="Nombres" name="txtNombre"></s:textfield>
				<s:textfield label="Apellidos" name="txtApellidos"></s:textfield>
				<s:textfield label="Edad" name="txtEdad"></s:textfield>
				<s:radio label="Género" name="rdGenero"
					list="#{'1': 'Masculino', '2': 'Femenino' }"></s:radio>
				<s:select label="Departamento" name="cboDepartamento" 
					list="#{ '1': 'CDMX', '2': 'Puebla', '3':'Acapulco', '4': 'Chilpancingo'}"
					headerKey="-1" headerValue="---Seleccionar---"></s:select>
				<s:submit value="Registrar"></s:submit>
				<s:reset value="Borrar"></s:reset>
			</s:form>
		</fieldset>
	</center>
	
</body>
</html>
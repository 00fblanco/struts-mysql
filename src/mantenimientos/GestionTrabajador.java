package mantenimientos;

import java.sql.Connection;
import java.sql.PreparedStatement;

import beans.Trabajador;
import utils.MySqlConexion;

public class GestionTrabajador {
	
	public void RegistrarEntradas(Trabajador data) {
		Connection conn = null;
		PreparedStatement pstm = null;
		
		try {
			conn = MySqlConexion.getConexion();
			String sql = "INSERT INTO TRABAJADOR VALUES(default, ?, ?, ?, ?, ?)";
			System.out.println(data.getNOMBRE());
			System.out.println(data.getAPELLIDO());
			System.out.println(data.getEDAD());
			System.out.println(data.getGENERO_COD());
			System.out.println(data.getDEPARTAMENTO_COD());
			pstm = conn.prepareStatement(sql);
				pstm.setString(1, data.getNOMBRE());
				pstm.setString(2, data.getAPELLIDO());
				pstm.setInt(3, data.getEDAD());
				pstm.setInt(4, data.getGENERO_COD());
				pstm.setInt(5, data.getDEPARTAMENTO_COD());
				
			pstm.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				if(pstm != null) {
					pstm.close();
				}
				if(conn != null) {
					conn.close();
				}
			}catch (Exception e2) {
				e2.printStackTrace();
			}
		}
	}
}

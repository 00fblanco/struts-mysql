package action;

import beans.Trabajador;
import mantenimientos.GestionTrabajador;

public class TrabajadorAction {
	private String txtNombre;
	private String txtApellidos;
	private int txtEdad;
	private int rdGenero;
	private int cboDepartamento;
	
	private Trabajador objTrabajador;
	
	public String ingresar() {
		objTrabajador = new Trabajador("", txtNombre, txtApellidos, txtEdad, rdGenero, cboDepartamento);
		
		try {
			GestionTrabajador gt = new GestionTrabajador();
			gt.RegistrarEntradas(objTrabajador);
			return "exito";
		} catch (Exception e) {
			e.printStackTrace();
			return "error";
		}
	}
	
	
	public String getTxtNombre() {
		return txtNombre;
	}
	public void setTxtNombre(String txtNombre) {
		this.txtNombre = txtNombre;
	}
	public String getTxtApellidos() {
		return txtApellidos;
	}
	public void setTxtApellidos(String txtApellidos) {
		this.txtApellidos = txtApellidos;
	}
	public int getTxtEdad() {
		return txtEdad;
	}
	public void setTxtEdad(int txtEdad) {
		this.txtEdad = txtEdad;
	}
	public int getRdGenero() {
		return rdGenero;
	}
	public void setRdGenero(int rdGenero) {
		this.rdGenero = rdGenero;
	}
	public int getCboDepartamento() {
		return cboDepartamento;
	}
	public void setCboDepartamento(int cboDepartamento) {
		this.cboDepartamento = cboDepartamento;
	}


	public Trabajador getObjTrabajador() {
		return objTrabajador;
	}


	public void setObjTrabajador(Trabajador objTrabajador) {
		this.objTrabajador = objTrabajador;
	}
	
	
}

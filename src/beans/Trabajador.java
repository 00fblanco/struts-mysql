package beans;

public class Trabajador {
	private String id;
	private String NOMBRE;
	private String APELLIDO;
	private int EDAD;
	private int GENERO_COD;
	private int DEPARTAMENTO_COD;
	
	public Trabajador(String id, String nOMBRE, String aPELLIDO, int eDAD, int gENERO_COD, int dEPARTAMENTO_COD) {
		super();
		this.id = id;
		NOMBRE = nOMBRE;
		APELLIDO = aPELLIDO;
		EDAD = eDAD;
		GENERO_COD = gENERO_COD;
		DEPARTAMENTO_COD = dEPARTAMENTO_COD;
	}

	public Trabajador() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNOMBRE() {
		return NOMBRE;
	}

	public void setNOMBRE(String nOMBRE) {
		NOMBRE = nOMBRE;
	}

	public String getAPELLIDO() {
		return APELLIDO;
	}

	public void setAPELLIDO(String aPELLIDO) {
		APELLIDO = aPELLIDO;
	}

	public int getEDAD() {
		return EDAD;
	}

	public void setEDAD(int eDAD) {
		EDAD = eDAD;
	}

	public int getGENERO_COD() {
		return GENERO_COD;
	}

	public void setGENERO_COD(int gENERO_COD) {
		GENERO_COD = gENERO_COD;
	}

	public int getDEPARTAMENTO_COD() {
		return DEPARTAMENTO_COD;
	}

	public void setDEPARTAMENTO_COD(int dEPARTAMENTO_COD) {
		DEPARTAMENTO_COD = dEPARTAMENTO_COD;
	}
	
	
	
}
